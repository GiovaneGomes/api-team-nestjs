import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {

  save(): string {
    return 'Team was save'
  }

  update(): string {
    return 'Team was update'
  }

  delete(): string {
    return 'Team was deleted'
  }

  getById(): string {
    return 'Team was get by id';
  }

  get(): string {
    return 'All the teams was get'
  }

}
