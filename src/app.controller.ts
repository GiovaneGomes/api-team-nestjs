import {Controller, Delete, Get, Post, Put} from '@nestjs/common';
import { AppService } from './app.service';

@Controller('/api/v1/team')
export class AppController {

  constructor(private readonly appService: AppService) { }

  @Post()
  save(): string {
    return this.appService.save();
  }

  @Put('/update')
  update(): string {
    return this.appService.update();
  }

  @Delete('/delete')
  delete(): string {
    return this.appService.delete();
  }

  @Get('/id')
  getHello(): string {
    return this.appService.getById();
  }

  @Get()
  getTeste(): string {
    return this.appService.get();
  }

}
